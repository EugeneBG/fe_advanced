function draw() {
    const canvas = document.getElementById('circle');

    if (canvas.getContext){
        const ctx = canvas.getContext('2d');

        ctx.strokeStyle = 'white';
        ctx.lineWidth = 4;

        ctx.arc(50, 50, 40, 0, Math.PI * 2)
        ctx.moveTo(35, 46);
        ctx.lineTo(50, 60);
        ctx.lineTo(65, 46);
        ctx.stroke();
    }
}
